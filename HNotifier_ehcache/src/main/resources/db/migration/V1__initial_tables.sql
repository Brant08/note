--role table

DROP TABLE IF EXISTS role;
CREATE TABLE role (
  id   INTEGER PRIMARY KEY,
  name VARCHAR(255)
);
DROP SEQUENCE IF EXISTS role_id_sequence;
CREATE SEQUENCE role_id_sequence;

--person_role table

DROP TABLE IF EXISTS person_role;
CREATE TABLE person_role (
  person_id INTEGER,
  role_id   INT NOT NULL,
  PRIMARY KEY (person_id, role_id)
);

--person table

DROP TABLE IF EXISTS person;
CREATE TABLE person (
  id          INTEGER PRIMARY KEY,
  first_name  VARCHAR(255),
  second_name VARCHAR(255),
  last_name   VARCHAR(255),
  login       VARCHAR(45),
  password    VARCHAR(45),
  location_id INTEGER NOT NULL
);
DROP SEQUENCE IF EXISTS person_id_sequence;
CREATE SEQUENCE person_id_sequence;

--person_group table
DROP TABLE IF EXISTS person_group;
CREATE TABLE person_group (
  group_id  INT,
  person_id INT
);

--studyGroup table

DROP TABLE IF EXISTS study_group;
CREATE TABLE study_group (
  id   INTEGER PRIMARY KEY,
  name VARCHAR(255)
);
DROP SEQUENCE IF EXISTS study_group_id_sequence;
CREATE SEQUENCE study_group_id_sequence;

--LOCATION_GROUP table

DROP TABLE IF EXISTS location_group;
CREATE TABLE location_group (
  location_id INTEGER,
  group_id    INTEGER
);

--location table

DROP TABLE IF EXISTS location;
CREATE TABLE location (
  id                 INTEGER PRIMARY KEY,
  city               VARCHAR(255),
  branch_office_name VARCHAR(255),
  address            VARCHAR(255)
);
DROP SEQUENCE IF EXISTS location_id_sequence;
CREATE SEQUENCE location_id_sequence;

--subscriptor_event table

DROP TABLE IF EXISTS subscriptor_event;
CREATE TABLE subscriptor_event (
  id                INTEGER,
  event_id          INTEGER,
  person_id         INTEGER,
  last_notification TIMESTAMP,
  PRIMARY KEY (id)
);
DROP SEQUENCE IF EXISTS subscriptor_id_sequence;
CREATE SEQUENCE subscriptor_id_sequence;
--event table

DROP TABLE IF EXISTS event;
CREATE TABLE event (
  id            INTEGER PRIMARY KEY,
  date_time     TIMESTAMP,
  duration      BIGINT,
  text          VARCHAR(500),
  event_type_id INTEGER NOT NULL,
  location_id   INTEGER
);
DROP SEQUENCE IF EXISTS event_id_sequence;
CREATE SEQUENCE event_id_sequence;

--event_type

DROP TABLE IF EXISTS event_type;
CREATE TABLE event_type (
  id   INTEGER PRIMARY KEY,
  name VARCHAR(255)
);
DROP SEQUENCE IF EXISTS event_type_id_sequence;
CREATE SEQUENCE event_type_id_sequence;

--message

DROP TABLE IF EXISTS message;
CREATE TABLE message (
  id          INTEGER,
  sender_id   INTEGER,
  receiver_id INTEGER,
  is_read     BOOLEAN,
  date        DATE,
  content     VARCHAR(500),
  PRIMARY KEY (id)
);
DROP SEQUENCE IF EXISTS message_id_sequence;
CREATE SEQUENCE message_id_sequence;
-------------
--reference
--
ALTER TABLE person_role
  ADD CONSTRAINT FK_PersoneRole_Role
FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE;

ALTER TABLE person_role
  ADD CONSTRAINT FK_PersonRole_Person
FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE;

ALTER TABLE event
  ADD CONSTRAINT FK_event_type
FOREIGN KEY (event_type_id) REFERENCES event_type (id) ON DELETE CASCADE;

ALTER TABLE person_group
  ADD CONSTRAINT FK_person_group
FOREIGN KEY (group_id) REFERENCES study_group (id) ON DELETE CASCADE;

ALTER TABLE person_group
  ADD CONSTRAINT FK_person_group_to_person
FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE;
