package com.controller;

import com.service.WebAppEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/entities")
public class WebAppEntityController {

  @Autowired
  WebAppEntityService webAppEntityService;

  @RequestMapping(value = "/hello", method = RequestMethod.GET)
  public String getHello() {
    return "index";
  }

  @RequestMapping(value = "/list", method = RequestMethod.GET)
  @ResponseBody
  public List getAllEntities() {
    return webAppEntityService.getAllEntities();
  }
}
