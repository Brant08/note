package com.controller;

import com.dto.EventDto;
import com.service.EventService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@Controller
@RequestMapping("/event")
public class EventController {

  @Autowired
  private EventService eventService;

  @RequestMapping(value = "/add", method = RequestMethod.PUT)
  public void add(@RequestBody @Valid EventDto dto) {
    eventService.addEvent(dto);
  }

  @RequestMapping(value = "/update", method = RequestMethod.POST)
  public void update(@RequestBody @Valid EventDto dto) {
    eventService.updateEvent(dto);
  }

  @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
  public void delete(
      @RequestParam("event_id")
      @NotNull(message = "event id should be specified") Integer eventId) {
    eventService.deleteEvent(eventId);
  }

  @RequestMapping(value = "/all_by_period_and_location", method = RequestMethod.POST)
  @ResponseBody
  public List<EventDto> findByPeriodAndLocation(
      @RequestParam(value = "location_id")
      @NotNull(message = "location id should be specified")
          Integer locationId,
      @RequestParam(value = "from")
      @DateTimeFormat(iso = ISO.DATE_TIME, pattern = "dd-MM-yyyy HH:mm")
      @NotNull(message = "start date should be specified")
          LocalDateTime from,
      @RequestParam(value = "to", required = false, defaultValue = "10-10-2100 10:10")
      @DateTimeFormat(iso = ISO.DATE_TIME, pattern = "dd-MM-yyyy HH:mm")
          LocalDateTime to) {
    return eventService.findByPeriodAndLocation(from.toLocalDate(), to.toLocalDate(), locationId);
  }

}
