package com.controller;

import com.model.Message;
import com.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class MessageController {

  @Autowired
  private MessageService messageService;

  @RequestMapping(value = "/add", method = RequestMethod.PUT)
  public void sendMessage(@RequestBody Message message) {
    messageService.addMessage(message);
  }
}
