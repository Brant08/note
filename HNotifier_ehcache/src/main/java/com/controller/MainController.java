package com.controller;

import com.model.*;
import java.security.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.authentication.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class MainController {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserDetailsService userDetailsService;

  @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
  public String index() {
    return "index";
  }

  @RequestMapping(value = "/registration", method = RequestMethod.GET)
  public String registration(Model model) {
    model.addAttribute("userForm", new Person());
    return "registration";
  }


  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String login(Model model, String error, String logout) {
    if (error != null) {
      model.addAttribute("error", "Your username and password is invalid.");
    }
    if (logout != null) {
      model.addAttribute("logout", "You have been logged out successfully.");
    }
    return "login";
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String login(Model model){
    model.addAttribute("name", "name");
    return "index";
  }

  @RequestMapping("/user")
  public String userIndex() {
    return "user/user-index";
  }


  @RequestMapping(value = "/403", method = RequestMethod.GET)
  public String accesDenied(Principal user, Model model) {
    if (user != null) {
      model.addAttribute("msg", "Hi " + user.getName()
          + ", you do not have permission to access this page!");
    } else {
      model.addAttribute("msg",
          "You do not have permission to access this page!");
    }
    return "403";
  }
}