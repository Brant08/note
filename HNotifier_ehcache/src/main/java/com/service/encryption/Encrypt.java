package com.service.encryption;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class Encrypt {

    public static String encryptPassword(String password, SecretKey secretKey) {

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] byteCipherPassword = cipher.doFinal(password.getBytes());
            return Base64.encode(byteCipherPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decryptPassword(String encryptPassword, SecretKey secretKey) {

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] bytePlainPassword = cipher.doFinal(Base64.decode(encryptPassword));
            return new String(bytePlainPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
