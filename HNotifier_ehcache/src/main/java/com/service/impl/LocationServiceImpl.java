package com.service.impl;

import com.dao.LocationDao;
import com.dto.LocationDto;
import com.exception.LocationNotFoundException;
import com.model.Location;
import com.service.LocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LocationServiceImpl implements LocationService {

  @Autowired
  private LocationDao locationDao;

  @Override
  public void addLocation(LocationDto locationDto) {
    locationDao.save(locationDto.buildLocation());
  }

  @Override
  public void updateLocation(LocationDto locationDto) {
    Location location = locationDao.findById(locationDto.id)
        .orElseThrow(() -> new LocationNotFoundException());
    location = locationDto.syncLocation(location);
    locationDao.update(location);
  }

  @Override
  public void deleteLocation(Integer locationId) {
    locationDao.delete(locationId);
  }

  @Override
  public List<LocationDto> getLocations() {
    return locationDao.findAll().stream()
        .map(loc -> new LocationDto(loc))
        .collect(Collectors.toList());
  }
}
