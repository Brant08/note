package com.service.impl;

import com.dao.EventDAO;
import com.dao.EventTypeDao;
import com.dao.LocationDao;
import com.dto.EventDto;
import com.exception.EventNotFoundException;
import com.exception.EventTypeNotFoundException;
import com.exception.LocationNotFoundException;
import com.model.Event;
import com.model.EventType;
import com.model.Location;
import com.service.EventService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {

  @Autowired
  private EventDAO eventDAO;
  @Autowired
  private EventTypeDao eventTypeDao;
  @Autowired
  private LocationDao locationDao;

  @Override
  public void addEvent(EventDto eventDto) {
    Event event = eventDto.buildEvent();
    Integer locationId = eventDto.locationId;
    Integer eventTypeId = eventDto.eventTypeId;

    if (locationId != null) {
      Location location = locationDao.findById(locationId)
          .orElseThrow(() -> new LocationNotFoundException());
      event.setLocation(location);
    }
    if (eventTypeId != null) {
      EventType eventType = eventTypeDao.findById(eventDto.eventTypeId)
          .orElseThrow(() -> new EventTypeNotFoundException());
      event.setEventType(eventType);
    }
    eventDAO.save(event);
  }

  @Override
  public void updateEvent(EventDto eventDto) {
    Event event = eventDAO.findById(eventDto.id).orElseThrow(() -> new EventNotFoundException());
    event = eventDto.syncEvent(event);
    Integer locationId = eventDto.locationId;
    Integer eventTypeId = eventDto.eventTypeId;

    if (locationId != null && locationId != event.getLocation().getId()) {
      Location location = locationDao.findById(locationId)
          .orElseThrow(() -> new LocationNotFoundException());
      event.setLocation(location);
    }
    if (eventTypeId != null && eventTypeId != event.getEventType().getId()) {
      EventType eventType = eventTypeDao.findById(eventTypeId)
          .orElseThrow(() -> new EventTypeNotFoundException());
      event.setEventType(eventType);
    }
    eventDAO.update(event);
  }

  @Override
  public boolean deleteEvent(Integer eventId) {
    return eventDAO.delete(eventId);
  }

  @Override
  public List<EventDto> findByPeriodAndLocation(LocalDate from, LocalDate to, Integer locationId) {
    Location location = locationDao.findById(locationId)
        .orElseThrow(() -> new LocationNotFoundException());
    return eventDAO.getAllBetweenTimeAndLocation(
        from.atStartOfDay(),
        to.plusDays(1).atStartOfDay(),
        location
    ).stream()
        .map(event -> new EventDto(event))
        .collect(Collectors.toList());
  }
}
