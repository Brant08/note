package com.service.impl;

import com.dao.*;
import com.model.*;
import com.service.*;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

  @Autowired
  private MessageDAO messageDAO;

  @Override
  public void addMessage(Message message) {
    messageDAO.save(message);
  }

  @Service
  public static class WebAppEntityServiceImpl implements WebAppEntityService {

    @Autowired
    WebAppEntityDAO webAppEntityDAO;

    public List<WebAppEntity> getAllEntities() {
      return webAppEntityDAO.getAllEntities();
    }
  }
}
