package com.service.impl;

import com.dao.EventTypeDao;
import com.dto.EventTypeDto;
import com.exception.EventTypeNotFoundException;
import com.model.EventType;
import com.service.EventTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventTypeServiceImpl implements EventTypeService {

  @Autowired
  private EventTypeDao eventTypeDao;

  @Override
  public void addEventType(EventTypeDto eventTypeDto) {
    eventTypeDao.save(eventTypeDto.buildEventType());
  }

  @Override
  public void updateEventType(EventTypeDto eventTypeDto) {
    EventType eventType = eventTypeDao.findById(eventTypeDto.id)
        .orElseThrow(() -> new EventTypeNotFoundException());
    eventTypeDao.update(eventTypeDto.syncEventType(eventType));
  }

  @Override
  public boolean deleteEventType(Integer eventTypeId) {
    return eventTypeDao.delete(eventTypeId);
  }

  @Override
  public List<EventTypeDto> getEventTypes() {
    return eventTypeDao.findAll().stream()
        .map(eventType -> new EventTypeDto(eventType))
        .collect(Collectors.toList());
  }
}
