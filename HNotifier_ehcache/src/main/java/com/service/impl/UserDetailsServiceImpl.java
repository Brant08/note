package com.service.impl;

import com.dao.*;
import com.model.*;
import java.util.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private PersonDAO personDAO;

  @Override
  @Transactional(readOnly = true)
  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

    Optional<Person> optionalPerson = personDAO.findByLogin(login);

    Person person;

    // check it
    if (optionalPerson.isPresent()) {
      person = optionalPerson.get();
    } else {
      throw new UsernameNotFoundException("User not found");
    }

    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    for (Role role : person.getRoles()) {
      grantedAuthorities.add(new SimpleGrantedAuthority(role.getName().toString()));
    }

    return new User(person.getLogin(),
        person.getPassword(), grantedAuthorities);
  }

}

