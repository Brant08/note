package com.service;

import com.dto.LocationDto;

import java.util.List;

public interface LocationService {

  void addLocation(LocationDto locationDto);

  void updateLocation(LocationDto locationDto);

  void deleteLocation(Integer locationId);

  List<LocationDto> getLocations();
}
