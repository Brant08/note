package com.dao;

import com.model.Location;
import com.model.Person;
import com.model.StudyGroup;

import java.util.List;
import java.util.Optional;

public interface GroupDao {

  Optional<StudyGroup> findById(Integer id);

  Optional<StudyGroup> findByNameAndLocation(String groupName, Location location);

  List<StudyGroup> findByPerson(Person person);

  List<StudyGroup> findAllByName(String groupName);

  StudyGroup update(StudyGroup studyGroup);

  void delete(StudyGroup studyGroup);

  StudyGroup save(StudyGroup studyGroup);
}
