package com.dao;

import com.model.WebAppEntity;

import java.util.List;

public interface WebAppEntityDAO {

  List getAllEntities();
}
