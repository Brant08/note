package com.dao;

import com.model.Location;
import com.model.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDAO {

  Person save(Person person);

  void delete(Person person);

  void update(Person person);

  Optional<Person> findById(Integer id);

  List<Person> findAll();

  Optional<Person> findByLogin(String login);

  List<Person> findAllByLocation(Location location);

  List<Person> findByLastName(String surname);

}
