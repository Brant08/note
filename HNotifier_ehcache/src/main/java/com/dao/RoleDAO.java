package com.dao;

import com.model.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;

public interface RoleDAO  {
  List<Role> findAll();
}
