package com.dao;

import com.model.Event;
import com.model.Location;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EventDAO {

  Optional<Event> findById(Integer id);

  List<Event> getAllByLocation(Location location);

  List<Event> getAllBetweenTimeAndLocation(LocalDateTime from, LocalDateTime to, Location location);

  List<Event> getAllTodayByLocation(Location location);

  List<Event> getAllToday();

  List<Event> getAllFutureByLocation(Location location);

  List<Event> getAllFuture();

  Event update(Event event);

  void delete(Event event);

  boolean delete(Integer eventId);

  Event save(Event event);
}
