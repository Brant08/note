package com.dao;

import com.model.EventType;

import java.util.List;
import java.util.Optional;

public interface EventTypeDao {

  Optional<EventType> findById(Integer id);

  Optional<EventType> findByName(String name);

  EventType update(EventType eventType);

  void delete(EventType eventType);

  boolean delete(Integer eventTypeId);

  List<EventType> findAll();

  EventType save(EventType eventType);

}
