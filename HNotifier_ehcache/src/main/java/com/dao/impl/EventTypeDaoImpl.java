package com.dao.impl;

import com.dao.EventTypeDao;
import com.exception.EventTypeNotFoundException;
import com.exception.EventTypeNotSpecifiedException;
import com.model.EventType;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional(readOnly = false)
@Repository
public class EventTypeDaoImpl implements EventTypeDao {

  @PersistenceContext
  private EntityManager em;

  @Transactional(readOnly = true)
  @Override
  @Cacheable(value = "findEventTypeById")
  public Optional<EventType> findById(Integer id) {
    if (id == null) {
      throw new EventTypeNotSpecifiedException();
    }
    EventType eventType = em.find(EventType.class, id);
    return Optional.ofNullable(eventType);
  }

  @Transactional(readOnly = true)
  @Override
  @Cacheable(value = "findEventTypeByName", key = "#name")
  public Optional<EventType> findByName(String name) {
    EventType result = em.createQuery("select et from EventType et "
        + "where et.name = ?1", EventType.class)
        .setParameter(1, name)
        .getSingleResult();
    return Optional.ofNullable(result);
  }

  @Override
  public EventType update(EventType eventType) {
    return em.merge(eventType);
  }

  @Override
  public void delete(EventType eventType) {
    EventType foundEventType = findById(eventType.getId())
        .orElseThrow(() -> new EventTypeNotFoundException());
    em.remove(foundEventType);
  }

  @Override
  public boolean delete(Integer eventTypeId) {
    return em.createQuery("delete from EventType as et where et.id = ?1")
        .setParameter(1, eventTypeId)
        .executeUpdate() > 0;
  }

  @Override
  @Cacheable(value = "findAllEventType", key = "#root.method.name")
  public List<EventType> findAll() {
    return em.createQuery("from EventType", EventType.class)
        .getResultList();
  }

  @Override
  public EventType save(EventType eventType) {
    return em.merge(eventType);
  }
}
