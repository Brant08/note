package com.dao.impl;

import com.dao.MessageDAO;
import com.model.Message;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
@Repository
public class MessageDAOImpl implements MessageDAO {

  @PersistenceContext
  private EntityManager em;

  @Override
  public Message save(Message message) {
    return em.merge(message);
  }
}
