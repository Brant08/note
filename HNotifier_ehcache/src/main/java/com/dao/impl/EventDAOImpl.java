package com.dao.impl;

import com.dao.EventDAO;
import com.exception.EventNotFoundException;
import com.exception.EventNotSpecifiedException;
import com.model.Event;
import com.model.Location;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional(readOnly = true)
@Repository
public class EventDAOImpl implements EventDAO {

  @PersistenceContext
  private EntityManager em;

  @Override
  @Cacheable(value = "findEventById")
  public Optional<Event> findById(Integer id) {
    if (id == null) {
      throw new EventNotSpecifiedException();
    }
    Event event = em.find(Event.class, id);
    return Optional.ofNullable(event);
  }

  @Override
  @Cacheable(value = "getAllEventsByLocation")
  public List<Event> getAllByLocation(Location location) {
    return em.createQuery(
        "select e from Event e "
            + " where e.location = ?1", Event.class)
        .setParameter(1, location)
        .getResultList();
  }

  @Override
  @Cacheable(value = "getAllEventsBetweenTimeAndLocation", key = "T(java.lang.String).valueOf(#from).concat('-').concat(#to).concat('-').concat(#location.id)")
  public List<Event> getAllBetweenTimeAndLocation(LocalDateTime from, LocalDateTime to,
      Location location) {
    return em.createQuery(
        "select e from Event e "
            + " where e.location = ?1 and e.dateTime between ?2 and ?3",
        Event.class)
        .setParameter(1, location)
        .setParameter(2, from)
        .setParameter(3, to)
        .getResultList();
  }

  @Override
  @Cacheable(value = "getAllEventsTodayByLocation")
  public List<Event> getAllTodayByLocation(Location location) {
    LocalDateTime from = LocalDate.now().atStartOfDay();
    LocalDateTime to = LocalDate.now().atStartOfDay().plusDays(1);
    return getAllBetweenTimeAndLocation(from, to, location);
  }

  @Override
  @Cacheable(value = "getAllEventsToday", key ="#root.method.name")
  public List<Event> getAllToday() {
    LocalDateTime from = LocalDate.now().atStartOfDay();
    LocalDateTime to = LocalDate.now().atStartOfDay().plusDays(1);
    return em.createQuery(
        "select e from Event e "
            + " where e.dateTime between ?1 and ?2",
        Event.class)
        .setParameter(1, from)
        .setParameter(2, to)
        .getResultList();
  }

  @Override
  @Cacheable(value = "getAllFutureEventsByLocation")
  public List<Event> getAllFutureByLocation(Location location) {
    LocalDateTime from = LocalDate.now().atStartOfDay().plusDays(1);
    return em.createQuery(
        "select e from Event e "
            + " where e.dateTime >= ?1 and e.location = ?2",
        Event.class)
        .setParameter(1, from)
        .setParameter(2, location)
        .getResultList();
  }

  @Override
  @Cacheable(value = "getAllFutureEvents", key ="#root.method.name")
  public List<Event> getAllFuture() {
    LocalDateTime from = LocalDate.now().atStartOfDay().plusDays(1);
    return em.createQuery(
        "select e from Event e "
            + " where e.dateTime >= ?1",
        Event.class)
        .setParameter(1, from)
        .getResultList();
  }

  @Transactional(readOnly = false)
  @Override
  public Event update(Event event) {
    return em.merge(event);
  }

  @Transactional(readOnly = false)
  @Override
  public void delete(Event event) {
    Event foundEvent = findById(event.getId()).orElseThrow(() -> new EventNotFoundException());
    em.remove(foundEvent);
  }

  @Override
  public boolean delete(Integer eventId) {
    return em.createQuery("delete from Event as e where e.id = ?1")
        .setParameter(1, eventId)
        .executeUpdate() > 0;
  }

  @Transactional(readOnly = false)
  @Override
  public Event save(Event event) {
    return em.merge(event);
  }
}
