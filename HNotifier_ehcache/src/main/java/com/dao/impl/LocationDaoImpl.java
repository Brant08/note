package com.dao.impl;

import com.dao.LocationDao;
import com.exception.LocationNotFoundException;
import com.exception.LocationNotSpecifiedException;
import com.model.Location;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional(readOnly = true)
@Repository
public class LocationDaoImpl implements LocationDao {

  @PersistenceContext
  private EntityManager em;

  @Override
  @Cacheable(value = "findLocationById")
  public Optional<Location> findById(Integer id) {
    if (id == null) {
      throw new LocationNotSpecifiedException();
    }
    Location location = em.find(Location.class, id);
    return Optional.ofNullable(location);
  }

  @Override
  @Cacheable(value = "findAllLocation", key="#root.method.name")
  public List<Location> findAll() {
    return em.createQuery("from Location", Location.class)
        .getResultList();
  }

  @Override
  @Cacheable(value = "findLocationByCityName", key = "#cityName")
  public List<Location> findByCityName(String cityName) {
    return em.createQuery("select loc from Location as loc where loc.city = ?1", Location.class)
        .setParameter(1, cityName).getResultList();
  }

  @Override
  @Cacheable(value = "findLocationByBranchName", key = "#branchName")
  public List<Location> findByBranchName(String branchName) {
    return em.createQuery("select loc from Location as loc where loc.branchOfficeName = ?1",
        Location.class)
        .setParameter(1, branchName)
        .getResultList();
  }

  @Transactional(readOnly = false)
  @Override
  public Location update(Location location) {
    return em.merge(location);
  }

  @Transactional(readOnly = false)
  @Override
  public void delete(Location location) {
    em.remove(location);
  }

  @Transactional(readOnly = false)
  @Override
  public boolean delete(Integer locationId) {
    return em.createQuery("delete from Location as loc where loc.id = ?1")
        .setParameter(1, locationId)
        .executeUpdate() > 0;
  }

  @Transactional(readOnly = false)
  @Override
  public Location save(Location location) {
    return em.merge(location);
  }
}
