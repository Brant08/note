package com.dao.impl;

import com.dao.GroupDao;
import com.exception.GroupNotFoundException;
import com.exception.GroupNotSpecifiedException;
import com.model.Location;
import com.model.Person;
import com.model.StudyGroup;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional(readOnly = true)
@Repository
public class GroupDaoImpl implements GroupDao {

  @PersistenceContext
  private EntityManager em;

  @Override
  @Cacheable(value = "findGroupById")
  public Optional<StudyGroup> findById(Integer id) {
    if (id == null) {
      throw new GroupNotSpecifiedException();
    }
    StudyGroup studyStudyGroup = em.find(StudyGroup.class, id);
    return Optional.ofNullable(studyStudyGroup);
  }

  @Override
  @Cacheable(value = "findGroupByNameAndLocation", key = "#groupName.concat('-').concat(#location.id)")
  public Optional<StudyGroup> findByNameAndLocation(String groupName, Location location) {
    StudyGroup result = em.createQuery(
        "select sg from StudyGroup sg"
            + " join sg.locations sgl"
            + " where sg.name = ?1 and sgl.id = ?2 ", StudyGroup.class)
        .setParameter(1, groupName)
        .setParameter(2, location.getId())
        .getSingleResult();
    return Optional.ofNullable(result);
  }

  @Override
  @Cacheable(value = "findGroupByPerson")
  public List<StudyGroup> findByPerson(Person person) {
    return em.createQuery("select sg from StudyGroup sg"
        + " join sg.people sgp "
        + " where sgp.id = ?1", StudyGroup.class)
        .setParameter(1, person.getId())
        .getResultList();
  }

  @Override
  @Cacheable(value = "findAllGroupByName", key="#groupName")
  public List<StudyGroup> findAllByName(String groupName) {
    return em.createQuery("select sg from StudyGroup sg where sg.name = ?1", StudyGroup.class)
        .setParameter(1, groupName)
        .getResultList();
  }

  @Transactional(readOnly = false)
  @Override
  public StudyGroup update(StudyGroup studyGroup) {
    return em.merge(studyGroup);
  }

  @Transactional(readOnly = false)
  @Override
  public void delete(StudyGroup studyGroup) {
    StudyGroup foundStudyGroup = findById(studyGroup.getId())
        .orElseThrow(() -> new GroupNotFoundException());
    em.remove(foundStudyGroup);
  }

  @Transactional(readOnly = false)
  @Override
  public StudyGroup save(StudyGroup studyGroup) {
    return em.merge(studyGroup);
  }
}
