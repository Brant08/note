//package com.security;
//
//import java.io.*;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import org.slf4j.*;
//import org.springframework.security.access.*;
//import org.springframework.security.core.*;
//import org.springframework.security.core.context.*;
//import org.springframework.security.web.access.*;
//import org.springframework.stereotype.*;
//
//@Component
//public class LoggingAccessDeniedHandler implements AccessDeniedHandler {
//
//  private static Logger log = LoggerFactory.getLogger(LoggingAccessDeniedHandler.class);
//
//  @Override
//  public void handle(HttpServletRequest request,
//      HttpServletResponse response,
//      AccessDeniedException ex) throws IOException, ServletException {
//
//    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//
//    if (auth != null) {
//      log.info(auth.getName()
//          + " was trying to access protected resource: "
//          + request.getRequestURI());
//    }
//
//    response.sendRedirect(request.getContextPath() + "/index");
//
//  }
//}
