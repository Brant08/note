package com.domain;

public enum AppRole {
  ROLE_ANONYMUS,
  ROLE_USER,
  ROLE_ADMIN
}
