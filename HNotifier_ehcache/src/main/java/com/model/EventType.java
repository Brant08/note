package com.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EVENT_TYPE")
public class EventType {

  @Id
  @SequenceGenerator(sequenceName = "event_type_id_sequence", name = "eventTypeIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventTypeIdSequence")
  @Column(name = "ID")
  private Integer id;

  @Column(name = "NAME", nullable = false, length = 255)
  private String name;

  @OneToMany(mappedBy = "eventType")
  private Set<Event> events = new HashSet<>();

  public void addEvent(Event event) {
    this.events.add(event);
    event.setEventType(this);
  }

  public void removeEvent(Event event) {
    this.events.add(event);
    event.setEventType(this);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Event> getEvents() {
    return events;
  }

  public void setEvents(Set<Event> events) {
    this.events = events;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    EventType eventType = (EventType) obj;

    if (id != eventType.id) {
      return false;
    }
    if (name != null ? !name.equals(eventType.name) : eventType.name != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    return result;
  }
}
