package com.model;

import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "PERSON")
public class Person {

  @Id
  @SequenceGenerator(sequenceName = "person_id_sequence", name = "personIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personIdSequence")
  @Column(name = "ID")
  private Integer id;

  @Column(name = "FIRST_NAME", length = 255)
  private String firstName;

  @Column(name = "SECOND_NAME", length = 255)
  private String secondName;

  @Column(name = "LAST_NAME", length = 255)
  private String lastName;

  @Column(name = "LOGIN", length = 255)
  private String login;

  @Column(name = "PASSWORD", length = 45)
  private String password;

  private String passwordConfirm;

  @ManyToOne
  @JoinColumn(name = "LOCATION_ID")
  private Location location;

  @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<SubscriptorEvent> subscriptorEvents = new HashSet<>();

  @ManyToMany(mappedBy = "people")
  private Set<StudyGroup> studyGroups = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "PERSON_ROLE",
      joinColumns = @JoinColumn(name = "PERSON_ID"),
      inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
  private Set<Role> roles = new HashSet<>();


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Transient
  public String getPasswordConfirm() {
    return passwordConfirm;
  }

  public void setPasswordConfirm(String passwordConfirm) {
    this.passwordConfirm = passwordConfirm;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Set<SubscriptorEvent> getSubscriptorEvents() {
    return subscriptorEvents;
  }

  public void setSubscriptorEvents(Set<SubscriptorEvent> subscriptorEvents) {
    this.subscriptorEvents = subscriptorEvents;
  }

  public Set<StudyGroup> getStudyGroups() {
    return studyGroups;
  }

  public void setStudyGroups(Set<StudyGroup> studyGroups) {
    this.studyGroups = studyGroups;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Person person = (Person) obj;
    return Objects.equals(firstName, person.firstName)
        && Objects.equals(secondName, person.secondName)
        && Objects.equals(lastName, person.lastName)
        && Objects.equals(login, person.login)
        && Objects.equals(password, person.password)
        && Objects.equals(location, person.location);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, secondName, lastName, login, password, location);
  }


  public static class Comparators {

    public static final Comparator<Person> LASTNAME =
        (Person o1, Person o2) -> o1.getLastName().compareToIgnoreCase(o2.getLastName());

    public static final Comparator<Person> FIRSTNAME =
        (Person o1, Person o2) -> o1.getFirstName().compareToIgnoreCase(o2.getFirstName());

    public static final Comparator<Person> LOGIN =
        (Person o1, Person o2) -> o1.getLogin().compareToIgnoreCase(o2.getLogin());
  }
}
