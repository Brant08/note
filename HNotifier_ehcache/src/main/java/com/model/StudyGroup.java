package com.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "STUDY_GROUP")
public class StudyGroup {

  @Id
  @SequenceGenerator(sequenceName = "study_group_id_sequence", name = "studyGroupIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "studyGroupIdSequence")
  @Column(name = "ID", nullable = false)
  private Integer id;

  @Column(name = "NAME", nullable = false, length = 255)
  private String name;

  @ManyToMany
  @JoinTable(name = "LOCATION_GROUP",
      joinColumns = @JoinColumn(name = "GROUP_ID"),
      inverseJoinColumns = @JoinColumn(name = "LOCATION_ID"))
  private Set<Location> locations = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "PERSON_GROUP",
      joinColumns = @JoinColumn(name = "GROUP_ID"),
      inverseJoinColumns = @JoinColumn(name = "PERSON_ID"))
  private Set<Person> people = new HashSet<>();


  public void addLocation(Location location) {
    this.locations.add(location);
    location.getStudyGroups().add(this);
  }

  public void removeLocation(Location location) {
    this.locations.remove(location);
    location.getStudyGroups().remove(this);
  }

  public void addPerson(Person person) {
    this.people.add(person);
    person.getStudyGroups().add(this);
  }

  public void removePerson(Person person) {
    this.people.remove(person);
    person.getStudyGroups().remove(this);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Person> getPeople() {
    return people;
  }

  public void setPeople(Set<Person> people) {
    this.people = people;
  }

  public Set<Location> getLocations() {
    return locations;
  }

  public void setLocations(Set<Location> locations) {
    this.locations = locations;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    StudyGroup groups = (StudyGroup) obj;
    return Objects.equals(name, groups.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}
