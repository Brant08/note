package com.model;

import com.domain.AppRole;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ROLE")
public class Role {

  @Id
  @SequenceGenerator(sequenceName = "role_id_sequence", name = "roleIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roleIdSequence")
  @Column(name = "ID")
  private int id;

  @Column(name = "NAME", nullable = false, length = 255)
  @Enumerated(EnumType.STRING)
  private AppRole name;

  @ManyToMany
  @JoinTable(name = "PERSON_ROLE",
      joinColumns = @JoinColumn(name = "ROLE_ID"),
      inverseJoinColumns = @JoinColumn(name = "PERSON_ID"))
  private Set<Person> persons = new HashSet<>();


  public void addPerson(Person person) {
    persons.add(person);
  }

  public void removePerson(Person person) {
    persons.remove(person);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public AppRole getName() {
    return name;
  }

  public void setName(AppRole name) {
    this.name = name;
  }

  public Set<Person> getPersons() {
    return persons;
  }


  public void setPersons(Set<Person> persons) {
    this.persons = persons;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Role role = (Role) obj;
    return name == role.name;
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}