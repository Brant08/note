package com.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.model.Location;

public class LocationDto {

  @JsonProperty(value = "ID")
  public Integer id;

  @JsonProperty(value = "CITY_NAME")
  public String city;

  @JsonProperty(value = "BRANCH_OFFICE_NAME")
  public String branchOfficeName;

  @JsonProperty(value = "ADDRESS")
  public String address;

  public LocationDto() {
  }

  public LocationDto(Location location) {
    this.id = location.getId();
    this.city = location.getCity();
    this.branchOfficeName = location.getBranchOfficeName();
    this.address = location.getAddress();
  }

  public Location buildLocation() {
    Location location = new Location();
    location.setCity(this.city);
    location.setBranchOfficeName(this.branchOfficeName);
    location.setAddress(this.address);
    return location;
  }

  public Location syncLocation(Location location) {
    if (this.city != null) {
      location.setCity(this.city);
    }
    if (this.address != null) {
      location.setAddress(this.address);
    }
    if (this.branchOfficeName != null) {
      location.setBranchOfficeName(this.branchOfficeName);
    }
    return location;
  }
}
