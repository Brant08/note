//package com.controller;
//
//import static org.hamcrest.CoreMatchers.equalTo;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//
//@RunWith(MockitoJUnitRunner.class)
//public class EntityControllerTest {
//
//  private RestTemplate restTemplate;
//
//  @Test
//  public void getHelloTest() {
//    restTemplate = new RestTemplate();
//    ResponseEntity<String> responseEntity = restTemplate.getForEntity(
//        "http://localhost:9999/entities/hello",
//        String.class);
//    Assert.assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.OK));
//  }
//
//  @Test
//  public void getListTest() {
//    restTemplate = new RestTemplate();
//    ResponseEntity<String> responseEntity = restTemplate.getForEntity(
//        "http://localhost:9999/entities/list",
//        String.class);
//    Assert.assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.OK));
//  }
//}
//
//
//
