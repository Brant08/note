package com.dao;

import static org.assertj.core.api.Assertions.assertThat;

import com.model.Location;
import com.model.Person;
import com.model.StudyGroup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/test_spring-application-context.xml")
@SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = "/db/migration/V1__initial_tables.sql")
})
@Transactional
public class StudyGroupDaoImplTest {

  @Autowired
  private GroupDao groupDao;
  @Autowired
  private LocationDao locationDao;

  private StudyGroup studyGroup;
  private Location location;
  private Person person;

  private final String groupName = "Java Elementary";
  private final String updatedGroupName = "Java EE";
  private final String cityName = "Odessa";

  @Before
  public void init() {
    studyGroup = new StudyGroup();
    studyGroup.setName(groupName);

    location = new Location();
    location.setCity(cityName);

    person = new Person();
  }

  @Test
  public void shouldSaveGroupAndGetItById() {
    studyGroup = groupDao.save(studyGroup);

    Optional<StudyGroup> foundGroup = groupDao.findById(studyGroup.getId());
    assertThat(foundGroup.isPresent()).isTrue();
    assertThat(foundGroup.get().getName()).isEqualTo(groupName);
  }


  @Test
  public void shoulUpdateAndThenGetItById() throws Exception {
    studyGroup = groupDao.save(studyGroup);
    studyGroup.setName(updatedGroupName);
    groupDao.update(studyGroup);

    Optional<StudyGroup> foundGroup = groupDao.findById(studyGroup.getId());
    assertThat(foundGroup.isPresent()).isTrue();
    assertThat(foundGroup.get().getName()).isEqualTo(updatedGroupName);
  }

  @Test
  public void shouldSaveAndGetListByGroupName() {
    groupDao.save(studyGroup);

    List<StudyGroup> allByName = groupDao.findAllByName(groupName);
    assertThat(allByName.size()).isEqualTo(1);
    assertThat(allByName.get(0).getName()).isEqualTo(groupName);
  }

  @Test
  public void shouldSaveAndThenDelete() {
    studyGroup = groupDao.save(studyGroup);
    groupDao.delete(studyGroup);
    Optional<StudyGroup> foundGroup = groupDao.findById(studyGroup.getId());
    assertThat(foundGroup.isPresent()).isFalse();
  }

  @Test
  public void shouldSaveAssociatedWithLocationAndThenGetItBackByNameAndLocation() {
    location = locationDao.save(location);
    studyGroup.addLocation(location);
    groupDao.save(studyGroup);

    Optional<StudyGroup> foundByNameAndLocation = groupDao
        .findByNameAndLocation(groupName, location);
    assertThat(foundByNameAndLocation.isPresent()).isTrue();
    assertThat(foundByNameAndLocation.get().getName()).isEqualTo(groupName);
    assertThat(foundByNameAndLocation.get().getLocations()).contains(location);

    List<StudyGroup> foundByPerson = groupDao.findByPerson(person);
    assertThat(foundByPerson.size()).isEqualTo(0);
  }
}