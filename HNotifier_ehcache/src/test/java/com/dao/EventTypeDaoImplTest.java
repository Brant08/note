package com.dao;

import com.model.EventType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/test_spring-application-context.xml")
@SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = "/db/migration/V1__initial_tables.sql")
})
@Transactional
public class EventTypeDaoImplTest {

  @Autowired
  private EventTypeDao eventTypeDao;

  private EventType eventType;

  private final String name = "webinar";


  @Before
  public void init() {
    eventType = new EventType();
    eventType.setName(name);
  }

  @Test
  public void shouldSaveAndGetItById() throws Exception {
    eventType = eventTypeDao.save(eventType);

    Optional<EventType> foundById = eventTypeDao.findById(eventType.getId());
    assertThat(foundById.isPresent()).isTrue();
    assertThat(foundById.get().getName()).isEqualTo(name);
  }

  @Test
  public void shouldSaveAndGetItByName() {
    eventType = eventTypeDao.save(eventType);

    Optional<EventType> foundName = eventTypeDao.findByName(name);
    assertThat(foundName.isPresent()).isTrue();
    assertThat(foundName.get().getName()).isEqualTo(name);
  }

}