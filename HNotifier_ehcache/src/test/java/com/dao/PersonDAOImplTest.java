package com.dao;

import static org.assertj.core.api.Assertions.assertThat;

import com.model.Location;
import com.model.Person;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/test_spring-application-context.xml")
@SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = "/db/migration/V1__initial_tables.sql")
})
@Transactional
public class PersonDAOImplTest {

  @Autowired
  private PersonDAO personDAO;
  @Autowired
  private LocationDao locationDAO;

  private Person person;
  private Person person2;
  private Location location;

  private final String firstName = "John";
  private final String secondName = "J.";
  private final String lastName = "Dou";
  private final String updLastName = "Jonson";
  private final String login = "darkjoe";
  private final String password = "i87dassd8978987asd";

  private final String firstName2 = "Robert";
  private final String lastName2 = "Singer";
  private final String login2 = "bobby";


  @Before
  public void init() {
    location = new Location();
    location.setBranchOfficeName("Hillel Odessa");
    location.setCity("Odessa");

    person = new Person();
    person.setFirstName(firstName);
    person.setLastName(lastName);
    person.setSecondName(secondName);
    person.setLogin(login);
    person.setPassword(password);

    person2 = new Person();
    person2.setFirstName(firstName2);
    person2.setLastName(lastName2);
    person2.setLogin(login2);
  }

  @Test
  public void saveAndFoundPersonByIdAndCheckInputedData() throws Exception {
    location = locationDAO.save(location);
    person.setLocation(location);
    person = personDAO.save(person);

    Optional<Person> foundPerson = personDAO.findById(person.getId());

    assertThat(foundPerson.get().getFirstName()).isEqualTo(firstName);
    assertThat(foundPerson.get().getSecondName()).isEqualTo(secondName);
    assertThat(foundPerson.get().getLastName()).isEqualTo(lastName);
    assertThat(foundPerson.get().getLogin()).isEqualTo(login);
    assertThat(foundPerson.get().getPassword()).isEqualTo(password);
    assertThat(foundPerson.get().getLocation().getId()).isEqualTo(location.getId());
  }

  @Test
  public void saveAndDeletePerson() throws Exception {
    location = locationDAO.save(location);
    person.setLocation(location);
    person = personDAO.save(person);

    Optional<Person> foundPerson = personDAO.findById(person.getId());
    assertThat(foundPerson.isPresent()).isTrue();

    personDAO.delete(person);

    foundPerson = personDAO.findById(person.getId());
    assertThat(foundPerson.isPresent()).isFalse();
  }

  @Test
  public void saveAndUpdatePerson() throws Exception {
    location = locationDAO.save(location);
    person.setLocation(location);
    person = personDAO.save(person);

    Optional<Person> foundPerson = personDAO.findById(person.getId());
    assertThat(foundPerson.get().getLastName()).isEqualTo(lastName);

    person.setLastName(updLastName);
    person.setSecondName(null);
    personDAO.update(person);

    foundPerson = personDAO.findById(person.getId());
    assertThat(foundPerson.get().getLastName()).isEqualTo(updLastName);
    assertThat(foundPerson.get().getSecondName()).isNullOrEmpty();
  }

  @Test
  public void saveAndfindAllSortByLastName() throws Exception {
    location = locationDAO.save(location);
    person.setLocation(location);
    person2.setLocation(location);
    personDAO.save(person);
    personDAO.save(person2);

    List<Person> foundPersons = personDAO.findAll();
    assertThat(foundPersons.size()).isEqualTo(2);
    assertThat(foundPersons.get(0).getLastName()).isEqualTo(lastName);
    assertThat(foundPersons.get(1).getLastName()).isEqualTo(lastName2);
  }

  @Test
  public void saveAndFindByLogin() throws Exception {
    location = locationDAO.save(location);
    person.setLocation(location);
    person = personDAO.save(person);

    Optional<Person> foundPerson = personDAO.findById(person.getId());
    assertThat(foundPerson.get().getLogin()).isEqualTo(login);
  }

  @Test
  public void findAllByLocation() throws Exception {
    location = locationDAO.save(location);
    person.setLocation(location);
    person2.setLocation(location);
    personDAO.save(person);
    personDAO.save(person2);

    List<Person> foundPersons = personDAO.findAllByLocation(location);
    assertThat(foundPersons.size()).isEqualTo(2);
  }

  @Test
  public void findByLastName() throws Exception {
    location = locationDAO.save(location);
    person.setLocation(location);
    person2.setLocation(location);
    personDAO.save(person);
    personDAO.save(person2);

    List<Person> foundPersons = personDAO.findByLastName(lastName);
    assertThat(foundPersons.size()).isEqualTo(1);
    assertThat(foundPersons.get(0).getLastName()).isEqualTo(lastName);
  }
}