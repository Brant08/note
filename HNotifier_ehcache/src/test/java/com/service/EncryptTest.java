package com.service;

import com.service.encryption.Encrypt;
import com.service.encryption.KeyGenerator;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.SecretKey;

public class EncryptTest {
    @Test
    public void encryptTest() {
        String password = "HNotifier2018";
        SecretKey secretKey = KeyGenerator.getSecretKey();
        System.out.println("Origin password: " + password);
        String encryptPassword = Encrypt.encryptPassword(password, secretKey);
        System.out.println("Encrypted password: " + encryptPassword);
        String decryptPassword = Encrypt.decryptPassword(encryptPassword, secretKey);
        System.out.println("Decrypted password: " + decryptPassword);

        Assert.assertEquals(decryptPassword, password);
    }
}
